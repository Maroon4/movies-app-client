import React from "react";
import { Form, Icon } from "antd";
import { Link } from "react-router-dom";

import { Button, Block, FormField } from "../../../components";

const success = false;

const RegisterForm = props => {
    const {
        values,
        touched,
        errors,
        handleChange,
        handleBlur,
        handleSubmit,
        isValid,
        isSubmitting
    } = props;

    // console.log(values);
    return (
        <div>
            <div className="auth__top">
                <h2>Реєстрація</h2>
                <p>Для входу в чат вам потрібно зареєструватися</p>
            </div>
            <Block>
                {!success ? (
                    <Form onSubmit={handleSubmit} className="login-form">
                        <FormField
                            name="email"
                            icon="mail"
                            placeholder="E-Mail"
                            handleChange={handleChange}
                            handleBlur={handleBlur}
                            touched={touched}
                            errors={errors}
                            values={values}
                        />

                        <FormField
                            name="fullname"
                            icon="user"
                            placeholder="Ваше ім'я та прізвище"
                            handleChange={handleChange}
                            handleBlur={handleBlur}
                            touched={touched}
                            errors={errors}
                            values={values}
                        />

                        <FormField
                            name="password"
                            icon="lock"
                            placeholder="Пароль"
                            type="password"
                            handleChange={handleChange}
                            handleBlur={handleBlur}
                            touched={touched}
                            errors={errors}
                            values={values}
                        />

                        <FormField
                            name="password_2"
                            icon="lock"
                            placeholder="Повторите пароль"
                            type="password"
                            handleChange={handleChange}
                            handleBlur={handleBlur}
                            touched={touched}
                            errors={errors}
                            values={values}
                        />

                        <Form.Item>
                            {isSubmitting && !isValid && <span>Ошибка!</span>}
                            <Button
                                disabled={isSubmitting}
                                onClick={handleSubmit}
                                type="primary"
                                size="large"
                            >
                                Зареєструватися
                            </Button>
                        </Form.Item>
                        <Link className="auth__register-link" to="/signin">
                            Увійти в аккаунт
                        </Link>
                    </Form>
                ) : (
                    <div className="auth__success-block">
                        <div>
                            <Icon type="info-circle" theme="twoTone" />
                        </div>
                        <h2>Підтвердити свій аккаунт</h2>
                        <p>
                            На Вашу пошту відправлений лист
                        </p>
                    </div>
                )}
            </Block>
        </div>
    );
};

export default RegisterForm;