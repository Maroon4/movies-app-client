import api from "../utils/api";


export default class CinemastoreService {


     async getAllMovies () {

         return await api.getAllMovies()

    }

};