import React from "react";
import { Route } from "react-router-dom";
import {Button, Icon} from "antd";


import {ChatInput, Status} from "../../components";
import {Dialogs, Messages } from "../../containers"


import "./Home.scss";
import "../../styles/layouts/_chat.scss"


const Home = () => {



    return (
        <section className="home">


            <div className="chat">
                <div className="chat__sidebar">
                    <div className="chat__sidebar-header">
                        <div>
                            <Icon type="team"/>
                            <span>Список діалогів</span>
                        </div>
                        <Button type="link" shape="circle" icon="form"/>
                    </div>



                    <div className="chat__sidebar-dialogs">

                        <Dialogs/>
                    </div>

                </div>
                <div className="chat__dialog">
                    <div className="chat__dialog-header">
                        <div className="chat__dialog-header-center">
                            <b className="chat__dialog-header-username">Микола Іванович</b>
                            <div className="chat__dialog-header-status">
                                <Status/>
                            </div>
                        </div>
                        <Button type="link" shape="circle" icon="ellipsis"/>
                    </div>
                    <div className="chat__dialog-messages">
                        <Messages/>

                    </div>
                    <ChatInput/>
                </div>
            </div>

        </section>
    );
}

export default Home;


// const dialogsJson = [
//
//
//     {
//         "id": "5e3a68f3988a7c12e08f4910",
//         "text": "Ipsum quis exercitation eiusmod esse anim id quis nisi fugiat velit sunt. Nisi ut consequat anim incididunt Lorem cupidatat esse occaecat pariatur consequat. Irure nostrud Lorem velit sint tempor voluptate do et qui duis veniam.",
//         "created_at": "Thu Oct 12 1972 08:51:08 GMT+0000 (UTC)",
//         "user": {
//             "id": "5e3a68f3aab01e47c7bed31f",
//             "fullname": "Leah Steele",
//             "avatar": null
//         }
//     },
//     {
//         "id": "5e3a68f3648e66c873327523",
//         "text": "Cillum laboris minim minim consequat adipisicing consectetur aliqua occaecat laboris ullamco excepteur et veniam. Aute enim quis eiusmod quis nulla voluptate sunt quis pariatur non in duis culpa. Velit id nulla cupidatat esse.",
//         "created_at": "Sun Dec 09 1973 14:00:07 GMT+0000 (UTC)",
//         "user": {
//             "id": "5e3a68f3d405b103fd435a0e",
//             "fullname": "Atkins Pope",
//             "avatar": null
//         }
//     },
//     {
//         "id": "5e3a68f3e65cf6749aa55693",
//         "text": "Incididunt dolor cillum magna sunt aliquip ex id ipsum. Ad dolore Lorem velit officia consequat incididunt. Et nostrud enim anim duis elit occaecat nulla nisi cillum culpa.",
//         "created_at": "Thu Jun 19 1997 03:19:21 GMT+0000 (UTC)",
//         "user": {
//             "id": "5e3a68f379bfd8b8cee8d0b2",
//             "fullname": "Becky Hebert",
//             "avatar": null
//         }
//     },
//     {
//         "id": "5e3a68f3d5bb9ba0aab5c219",
//         "text": "Qui Lorem sunt cupidatat Lorem ut ad non. Quis tempor aute excepteur nostrud Lorem in esse et dolore. Incididunt non tempor pariatur voluptate proident mollit commodo aute sint excepteur minim.",
//         "created_at": "Sat Jun 27 2015 19:35:06 GMT+0000 (UTC)",
//         "user": {
//             "id": "5e3a68f308107fed3ff06e4e",
//             "fullname": "Randall Wise",
//             "avatar": null
//         }
//     },
//     {
//         "id": "5e3a68f34b995a31e570a983",
//         "text": "Cillum ipsum qui reprehenderit cupidatat consectetur voluptate aliqua ut nulla. Amet cillum officia ut veniam nostrud dolore consequat sit sint aliqua in minim. Adipisicing ad tempor ad laboris.",
//         "created_at": "Thu Mar 30 2006 21:18:56 GMT+0000 (UTC)",
//         "user": {
//             "id": "5e3a68f34fd8ab6e5259e898",
//             "fullname": "Lee Robinson",
//             "avatar": null
//         }
//     },
//     {
//         "id": "5e3a68f35e82d1bfcf0a0b50",
//         "text": "Duis deserunt fugiat do quis anim. Nisi duis nulla ea exercitation officia duis nulla proident exercitation sunt. Cupidatat eiusmod mollit officia commodo veniam ullamco deserunt laborum id Lorem cillum duis.",
//         "created_at": "Fri Jun 25 2004 07:58:40 GMT+0000 (UTC)",
//         "user": {
//             "id": "5e3a68f3972b473116c82df5",
//             "fullname": "Mills Spence",
//             "avatar": null
//         }
//     },
//     {
//         "id": "5e3a68f3aed40f979fdaf830",
//         "text": "Non elit do consectetur consequat ex cupidatat et Lorem. Adipisicing reprehenderit et dolor tempor proident eiusmod sunt excepteur duis non excepteur exercitation anim proident. Veniam laboris ex esse anim.",
//         "created_at": "Thu Sep 25 2008 10:31:47 GMT+0000 (UTC)",
//         "user": {
//             "id": "5e3a68f35e3f236401dadc1f",
//             "fullname": "Acevedo Newman",
//             "avatar": null
//         }
//     },
//     {
//         "id": "5e3a68f3bfda058bff983358",
//         "text": "Voluptate in et officia aliquip cillum ullamco cupidatat ut ad commodo sint commodo. Consequat est eu exercitation voluptate enim laborum ad qui voluptate aliqua ullamco dolore. Non consectetur in amet commodo qui sint eiusmod eu.",
//         "created_at": "Fri Feb 06 1976 15:38:49 GMT+0000 (UTC)",
//         "user": {
//             "id": "5e3a68f30e895f67b43e01db",
//             "fullname": "Hansen Hyde",
//             "avatar": null
//         }
//     },
//     {
//         "id": "5e3a68f31e2ba2d5750cdc5c",
//         "text": "Dolor ea amet deserunt eu et reprehenderit cupidatat. Proident dolor aute fugiat minim. Eu eu sit commodo quis.",
//         "created_at": "Thu Sep 09 1976 01:54:38 GMT+0000 (UTC)",
//         "user": {
//             "id": "5e3a68f339dedbaabe94abdb",
//             "fullname": "Langley Oneal",
//             "avatar": null
//         }
//     },
//     {
//         "id": "5e3a68f36349dcbd6fe8e496",
//         "text": "Reprehenderit sint quis ea est. Tempor aute eu consequat irure ex id duis cupidatat minim aute minim. Sint ex minim et duis consequat et commodo mollit.",
//         "created_at": "Tue Feb 01 2011 23:44:51 GMT+0000 (UTC)",
//         "user": {
//             "id": "5e3a68f3cafb2909c364f286",
//             "fullname": "Jimenez Fischer",
//             "avatar": null
//         }
//     },
//     {
//         "id": "5e3a68f3fabf4024d00e446b",
//         "text": "Officia excepteur laboris consequat laboris in qui reprehenderit labore proident. Exercitation mollit adipisicing qui esse elit cupidatat aliqua fugiat laboris ad reprehenderit magna. Consectetur aliquip cillum enim id aute mollit velit culpa consectetur labore amet dolor commodo fugiat.",
//         "created_at": "Tue Jul 17 2001 20:03:00 GMT+0000 (UTC)",
//         "user": {
//             "id": "5e3a68f3fe11247e41dd2ae6",
//             "fullname": "Bush Harding",
//             "avatar": null
//         }
//     },
//     {
//         "id": "5e3a68f36fd0871985772cd3",
//         "text": "Est nisi do do enim id. Fugiat incididunt occaecat nisi laborum dolor pariatur nulla amet sunt in deserunt. Quis mollit aliqua enim irure ut mollit id.",
//         "created_at": "Sat Mar 10 2012 03:12:38 GMT+0000 (UTC)",
//         "user": {
//             "id": "5e3a68f36164b5a8b9b8cd01",
//             "fullname": "Brennan Alford",
//             "avatar": null
//         }
//     },
//     {
//         "id": "5e3a68f3c3e4c85cc87c45a7",
//         "text": "Laboris incididunt nulla non anim deserunt. Ipsum culpa aute ea ad excepteur eiusmod tempor. Magna irure nisi deserunt consequat nostrud excepteur excepteur Lorem duis veniam tempor.",
//         "created_at": "Mon Mar 13 2006 14:43:16 GMT+0000 (UTC)",
//         "user": {
//             "id": "5e3a68f31be11794944e51a4",
//             "fullname": "Burgess Conway",
//             "avatar": null
//         }
//     },
//     {
//         "id": "5e3a68f314adb2ffb39a54b1",
//         "text": "Consequat commodo qui minim aliqua quis deserunt consectetur ut incididunt deserunt tempor. Et velit excepteur labore exercitation. Exercitation sunt aliquip consequat veniam velit cupidatat commodo minim id voluptate.",
//         "created_at": "Sat Jun 24 2017 05:45:30 GMT+0000 (UTC)",
//         "user": {
//             "id": "5e3a68f363fe269d34242009",
//             "fullname": "Herman Glass",
//             "avatar": null
//         }
//     },
//     {
//         "id": "5e3a68f373622a34bac2c25b",
//         "text": "Minim cupidatat irure id anim. Culpa duis exercitation consequat non sunt exercitation ullamco ea anim. Cillum labore sit duis ipsum pariatur cupidatat velit adipisicing cupidatat commodo.",
//         "created_at": "Fri Aug 08 1997 08:32:35 GMT+0000 (UTC)",
//         "user": {
//             "id": "5e3a68f344cd87b50985c355",
//             "fullname": "Susanne Atkins",
//             "avatar": null
//         }
//     },
//     {
//         "id": "5e3a68f32bd446959199728a",
//         "text": "Irure enim adipisicing commodo incididunt consequat est aliquip. Non ullamco nostrud duis consectetur. Irure proident Lorem dolor amet reprehenderit voluptate anim adipisicing fugiat elit minim ex culpa.",
//         "created_at": "Thu Aug 06 1992 05:41:14 GMT+0000 (UTC)",
//         "user": {
//             "id": "5e3a68f3858d229279796b7c",
//             "fullname": "Alyson Perkins",
//             "avatar": null
//         }
//     },
//     {
//         "id": "5e3a68f31aae1c8f98f9d230",
//         "text": "Id ut irure quis quis ipsum cillum minim nostrud adipisicing. Mollit eiusmod amet duis laboris. Anim ut mollit culpa sit consectetur consectetur.",
//         "created_at": "Sun Feb 09 2014 17:23:06 GMT+0000 (UTC)",
//         "user": {
//             "id": "5e3a68f327e415d81d9bcb9b",
//             "fullname": "Ila Odom",
//             "avatar": null
//         }
//     },
//     {
//         "id": "5e3a68f3e3f04e276e0565e6",
//         "text": "Nulla aliquip non ad esse incididunt. In exercitation ut aliquip nostrud nisi elit et magna ex incididunt commodo laborum. Irure et magna voluptate id nulla sunt nostrud irure aute nulla aliqua Lorem.",
//         "created_at": "Thu Nov 14 1974 02:23:22 GMT+0000 (UTC)",
//         "user": {
//             "id": "5e3a68f39926fece2c1e3dfe",
//             "fullname": "Knox Sweet",
//             "avatar": null
//         }
//     }
// ]