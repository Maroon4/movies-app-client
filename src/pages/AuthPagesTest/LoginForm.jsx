import React, { Component } from 'react'
import api from '../../utils/api'

import styled from 'styled-components'
import {Link} from "react-router-dom";

const Title = styled.h1.attrs({
    className: 'h1',
})``;

const Wrapper = styled.div.attrs({
    className: 'form-group',
})`
    margin: 0 30px;
`;

const Label = styled.label`
    margin: 5px;
`;

const InputText = styled.input.attrs({
    className: 'form-control',
})`
    margin: 5px;
`;

const Button = styled.button.attrs({
    className: `btn btn-primary`,
})`
    margin: 15px 15px 15px 5px;
`;

const CancelButton = styled.a.attrs({
    className: `btn btn-danger`,
})`
    margin: 15px 15px 15px 5px;
`;
const Item = styled.div.attrs({
    className: 'collpase navbar-collapse',
})``;

class LoginForm extends Component {
    constructor(props) {
        super(props);

        this.state = {
            id: this.props.match.params.id,
            username: '',
            password: '',
            isLogin: false
        }
    }

    handleChangeInputUserName = async event => {
        const username = event.target.value;
        this.setState({ username })
    };

    handleChangeInputPassword = async event => {
        const password = event.target.validity.valid
            ? event.target.value
            : this.state.password;

        this.setState({ password })
    };



    handleLoginUser = async () => {
        const { username, password} = this.state;
        // console.log(username, password);
        const payload = { username, password };

        await api.loginUser(payload).then((res) => {
            // window.alert(`Signin successfully`);
            console.log(res);
            if (res.status === 200) {
                window.alert(`Signin successfully`);
                this.setState({
                    isLogin: true
                })

            }
            // this.setState({
            //     username: '',
            //     password: ''
            // });
            console.log(this.state.isLogin)
        }, function (err) {
            if (err.response.status === 401) {
                window.alert(`Password is false`);
            }
            if (err.response.status === 404) {
                window.alert(`Undefined login or password`);
            }
            return Promise.reject(err.response)
        })


    };

    componentDidMount = async () => {

    };

    render() {
        // const { name, rating, time } = this.state;
        return (
            <Wrapper>
                <Title>Sign in</Title>

                <Label>UserName: </Label>
                <InputText
                    type="text"
                    // value=""
                    onChange={this.handleChangeInputUserName}
                />

                <Label>Password: </Label>
                <InputText
                    // type="number"
                    // step="0.1"
                    // lang="en-US"
                    // min="0"
                    // max="10"
                    // pattern="[0-9]+([,\.][0-9]+)?"
                    // value=""
                    onChange={this.handleChangeInputPassword}
                />

                <Item>
                    <Link to="/auth/signup">
                        Sign up
                    </Link>
                </Item>

                <Button
                    // href={'/movies/list'}
                    onClick={this.handleLoginUser}
                >Sign in</Button>
                <CancelButton href={'/movies/list'}>Cancel</CancelButton>
            </Wrapper>
        )
    }
}

export default LoginForm