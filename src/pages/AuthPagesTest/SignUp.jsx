import React, { Component } from 'react';

import api from '../../utils/api'

import styled from 'styled-components'

const Title = styled.h1.attrs({
    className: 'h1',
})``;

const Wrapper = styled.div.attrs({
    className: 'form-group',
})`
    margin: 0 30px;
`;

const Label = styled.label`
    margin: 5px;
`;

const InputText = styled.input.attrs({
    className: 'form-control',
})`
    margin: 5px;
`;

const Button = styled.button.attrs({
    className: `btn btn-primary`,
})`
    margin: 15px 15px 15px 5px;
`;

const CancelButton = styled.a.attrs({
    className: `btn btn-danger`,
})`
    margin: 15px 15px 15px 5px;
`;



class SignUp extends Component {
    constructor(props) {
        super(props);

        this.state = {
            id: this.props.match.params.id,
            name: '',
            username: '',
            password: '',
            email: '',
            roles: ['user']
        }
    }

    handleChangeInputName = async event => {
        const name = event.target.value;
        this.setState({name});
    };

    handleChangeInputUserName = async event => {
        const username = event.target.value;
        this.setState({ username })
    };

        handleChangeInputEmail = async event => {
            const email = event.target.value;
            this.setState({ email })
    };


    handleChangeInputPassword = async event => {
        const password = event.target.validity.valid
            ? event.target.value
            : this.state.password;

        this.setState({ password })
    };



    handleLoginUser = async () => {
        const { name, username, email, password, roles} = this.state;
        // console.log(username, password);
        const payload = { name, username, email, password, roles};

        await api.signUp(payload).then(res => {
            window.alert(`Signup successfully`);
            this.setState({
                name: '',
                username: '',
                email: '',
                password: '',
                roles: ['user']


            });
            console.log(this.state)
        })


    };

    componentDidMount = async () => {

    };

    render() {
        // const { name, time } = this.state;
        return (
            <Wrapper>
                <Title>Signup form</Title>

                <Label>Name: </Label>
                <InputText
                    type="text"
                    // value=""
                    onChange={this.handleChangeInputName}
                />

                <Label>UserName: </Label>
                <InputText
                    type="text"
                    // value=""
                    onChange={this.handleChangeInputUserName}
                />

                <Label>Email: </Label>
                <InputText
                    type="text"
                    // value=""
                    onChange={this.handleChangeInputEmail}
                />

                <Label>Password: </Label>
                <InputText
                    // type="number"
                    // step="0.1"
                    // lang="en-US"
                    // min="0"
                    // max="10"
                    // pattern="[0-9]+([,\.][0-9]+)?"
                    // value=""
                    onChange={this.handleChangeInputPassword}
                />

                <Button
                    // href={'/movies/list'}
                    onClick={this.handleLoginUser}
                >Sign up</Button>
                <CancelButton href={'/auth/signin'}>Cancel</CancelButton>
            </Wrapper>
        )
    }
}

export default SignUp