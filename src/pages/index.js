import MoviesList from './MoviesPages/MoviesList'
import MoviesInsert from './MoviesPages/MoviesInsert'
import MoviesUpdate from './MoviesPages/MoviesUpdate'

import LoginForm from "./AuthPagesTest/LoginForm";
import SignUp from "./AuthPagesTest/SignUp";



export {
    MoviesList,
    MoviesInsert,
    MoviesUpdate,
    LoginForm,
    SignUp
}