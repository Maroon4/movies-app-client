import React from 'react'
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'

import { NavBar } from '../components'
import { MoviesList, MoviesInsert, MoviesUpdate } from '../pages'
import Auth from "../pages/Auth";
import Home from "../pages/Home";

import 'bootstrap/dist/css/bootstrap.min.css'

const App = () => {
    return (

        <div className="-wrapper-body">

            <NavBar />
            {/*<MoviesList/>*/}
            <Switch>
                {/*<Route path="/movies/list" exact component={MoviesList} />*/}
                {/*<Route path="/movies/create" exact component={MoviesInsert} />*/}
                {/*<Route*/}
                {/*    path="/movies/update/:id"*/}
                {/*    exact*/}
                {/*    component={MoviesUpdate}*/}
                {/*/>*/}
                <Route path={["/signin", "/signup", "signup/verify"]} exact component={Auth}/>
                <Route path="/im" exact component={Home}/>

            </Switch>

        </div>
    )
}

export default App