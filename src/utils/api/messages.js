import axios from "../../core";

export default {
    getAllByDialogId: id => axios.get("/messages?dialog=" + id)

    // getAllByDialogId: () => axios.get("/messages" )

}
