import React, {useState, useEffect} from "react";
import {connect} from "react-redux"

import {dialogsActions} from "../redux/actions";
import {Dialogs as BaseDialog} from "../components";

const Dialogs = ({ fetchDialogs, currentDialogId, setCurrentDialogId, items, userId }) => {
    const [inputValue, setValue] = useState("");
    const [filtred, setFiltredItems] = useState(Array.from(items));
    // let filtred = Array.from(items);

    console.log(items)

    const onChangeInput = value => {
        console.log(value);
        setFiltredItems(filtred.filter(
            dialog => dialog.user.fullname.toLowerCase().indexOf(value) >= 0
        ));
        setValue(value)
    };


    useEffect(() => {
        if (!items.length) {
            fetchDialogs()
        } else {
            setFiltredItems(items)
        }

        console.log(items)
    }, [items]);

    return (
        <BaseDialog
            userId={userId}
            items={filtred}
            onSearch={onChangeInput}
            inputValue={inputValue}
            onSelectDialod={setCurrentDialogId}
            currentDialogId={currentDialogId}
        />
    )
};

export default connect(({dialogs}) => dialogs, dialogsActions)(Dialogs);