import React from "react";
import classNames from "classnames"
import {Time, IconReaded} from "../index" ;
import orderBy from "lodash/orderBy"

import "./Dialogs.scss"
import DialogItem from "../DialogItem";
import {Input, Empty} from "antd";



const Dialogs = ({items, userId, onSearch, inputValue, currentDialogId, onSelectDialod}) => {
    console.log(items.length);

    return (
    <div className="dialogs">
        <div className="dialogs__search">
            <Input.Search
                placeholder="Пошук діалогу"
                onChange={ e => onSearch(e.target.value)}
                style={{width: 290}}
                value={inputValue}
            />
        </div>
        {
          items.length ? (
              orderBy(items, ["created_at"], ["desc"]).map(item => (
                <DialogItem
                    key={item._id}
                    isMe={item.user._id === userId}
                    {...item}
                    user={item.user}
                    message={item}
                    onSelect={onSelectDialod}
                    currentDialogId={currentDialogId}
                    // unreaded={0}
                />
            ))
              ) : (<Empty image={Empty.PRESENTED_IMAGE_SIMPLE} description="Нічого не знайдено" />
            )}
    </div>
);
};

export default Dialogs;