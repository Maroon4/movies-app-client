import React from "react";
import {CinemastoreServiceConsumer} from "../cinema-service-context";

const withCinemastoreService = () => (Wrapped) => {
  return (props) => {
      return (
          <CinemastoreServiceConsumer>
              {
                  (cinemastoreService) => {
                      return (
                          <Wrapped {...props}
                                   cinemastoreService={cinemastoreService} />)


                  }
              }
          </CinemastoreServiceConsumer>
      );
  }
};

export default withCinemastoreService;