import React from "react";
import classNames from "classnames"

import {IconReaded} from "../index" ;
import format from 'date-fns/format'
import isToday from "date-fns/isToday"
import {generateAvatarFromHash} from "../../utils/helpers";

// import "./DialogItem.scss"
import Avatar from "../Avatar";


const getMessageTime = created_at => {


    console.log(created_at);
    if (isToday(created_at)) {
        // console.log(created_at);
      return format( created_at,'HH:mm')
  } else {
        return format( new Date(created_at),'dd.MM.yyyy')
    }
};

// const getAvatar = avatar => {
//
//     return (<Avatar);
//
// };

const DialogItem = ({
     _id,
     user,
     message,
     unreaded,
     isMe,
     onSelect,
     currentDialogId

}) => (
    <div className={classNames("dialogs__item", {
        "dialogs__item--online":user.isOnline,
        "dialogs__item--selected": currentDialogId === _id
    })}
    onClick={onSelect.bind(this, _id)}
    >
        <div className="dilogs__item-avatar">
            {/*<img src={user.avatar} alt={`${user.fullname} avatar`} />*/}
           <Avatar user={user}/>
        </div>
        <div className="dialog__item-info">
            <div className="dialogs__item-info-top">
              <b>{user.fullname}</b>
                <span>
                    {/*13:30*/}
                    {/*<Time date={message.created_at}/>*/}
                    {getMessageTime(message.created_at)}
                </span>
            </div>
            <div className="dialogs__item-info-bottom">
                <p>{message.text}</p>
                {isMe && <IconReaded isMe={true} isReaded={false}/>}
                {unreaded > 0 && (
                    <div className="dialogs__item-info-bottom-count">
                       {unreaded > 9 ? "+9" : unreaded}
                    </div>
                )}
            </div>
        </div>
    </div>
);

export default DialogItem;