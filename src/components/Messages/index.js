import React from "react";
import  PropTypes from "prop-types"
import {Empty, Spin, Alert} from "antd";
import {Message} from "../";

import "./Messages.scss"


const Messages = ({ blockRef, isLoading, items }) => {

    return (
        <div
            ref={blockRef}
            className="messages"
        >
            {
                isLoading ? (
                    <Spin
                        tip="Loading..."
                        size="large"
                    />
                ) : items && !isLoading ? (
                        items.length > 0 ? (items.map(item => (
                                <Message
                                    key={item._id}
                                    {...item}
                                />
                            ))
                            ) : (<Empty image={Empty.PRESENTED_IMAGE_SIMPLE} description="Діалог порожній" />)

                ) : (<Empty image={Empty.PRESENTED_IMAGE_SIMPLE} description="Відкрийте діалог" />)
            }

    </div>
    );





};


Messages.prototype = {

    items: PropTypes.array,

};
export default Messages;