import React, {Fragment} from "react";
import  PropTypes from "prop-types"
import formatDistanceToNow from "date-fns/formatDistanceToNow"
import parseISO from "date-fns/parseISO"
import ualocate from "date-fns/locale/ru"

const Time = ({date}) => {
   console.log(date);
    return (
        <Fragment>
            {formatDistanceToNow(new Date(date), {addSuffix: true, locale: ualocate})}
        </Fragment>
    );

};

Time.prototype = {
    date: PropTypes.string
};

export default Time;