import React, {useState, useEffect, useRef} from "react";
import PropTypes, {object} from 'prop-types'
import classNames from 'classnames';
import formatDistanceToNow from "date-fns/formatDistanceToNow"
import ualocate from "date-fns/locale/ru"
import { Picker, Emoji } from 'emoji-mart'

import readed from "../../assets/img/readed.svg"
import noreaded from "../../assets/img/noreaded.svg"
import waveSvg from "../../assets/img/wave.svg"
import playSvg from "../../assets/img/play.svg"
import pauseSvg from "../../assets/img/pause.svg"

import Time from "../Time";
import {IconReaded} from "../index";
import {isAudio, convertCurrentTime} from "../../utils/helpers"

import './Message.scss'
import Avatar from "../Avatar";

const MessageAudio = ({audioSrc}) => {

    const audioElem = useRef(null);
    const [isPlaying, setIsPlaying] = useState(false);
    const [progress, setProgress] = useState(0);
    const [currentTime, setCurrentTime] = useState(0);

    const togglePlay = () => {
        if (!isPlaying) {
            audioElem.current.play();
        } else {
            audioElem.current.pause();
        }
    };

    useEffect(() => {
        audioElem.current.volume = '0.5';
        audioElem.current.addEventListener(
            'playing',
            () => {
                setIsPlaying(true);
            },
            false,
        );
        audioElem.current.addEventListener(
            'ended',
            () => {
                setIsPlaying(false);
                setProgress(0);
                setCurrentTime(0);
            },
            false,
        );
        audioElem.current.addEventListener(
            'pause',
            () => {
                setIsPlaying(false);
            },
            false,
        );
        audioElem.current.addEventListener('timeupdate', () => {
            const duration = (audioElem.current && audioElem.current.duration) || 0;
            setCurrentTime(audioElem.current.currentTime);
            setProgress((audioElem.current.currentTime / duration) * 100);
        });
    }, []);

    return (
        <div className="message__audio ">
            <audio
                ref={audioElem}
                src={audioSrc}
                preload="auto"></audio>

            <div
                className="message__audio-progress"
                style={{widh: "40%" + "%"}}>
                <div className="message__audio-info">
                    <div className="message__audio-btn">
                        <button onClick={togglePlay}>
                            {isPlaying ?
                                <img src={pauseSvg} alt="pauseSvg"/> : <img src={playSvg} alt="playSvg"/>}
                        </button>
                    </div>
                    <div className="message__audio-wave">
                        <img src={waveSvg} alt="Wave svg"/>
                    </div>
                    <span className="message__audio-duration">{convertCurrentTime(currentTime)}</span>
                </div>
            </div>
        </div>
    )
};


const Message = ({
    avatar,
    user,
    text,
    date,
    audio,
    isMe,
    isReaded,
    attachments,
    isTyping

}) => {




    return (
       <div
           className={classNames("message", {
                       "message--isme": isMe,
                       "message--is-typing": isTyping,
                       "message--is-audio": audio,
                       "message--image": attachments && attachments.lenght ===1,

                 })}>
          <div className="message__content">
             <IconReaded isMe={isMe} isReaded={isReaded}/>
              {/*{ isMe ===true && isReaded === true ? (*/}
              {/*    <img className="message__icon-readed" src={readed} alt="Readed icon" />*/}
              {/*    ) : isMe===true && (*/}
              {/*    <img className="message__icon-readed message__icon-readed--no  " src={noreaded} alt="Not readed icon" />*/}
              {/*  )*/}
              {/*}*/}
           <div className="message__avatar">
               <Avatar user={user}/>
           </div>
           <div className="message__info">
               {(audio || text || isTyping) && (
               <div className="message__bubble">
                   {text && <p className="message__text">
                       {text}
                       {/*<Emoji emoji=':santa::skin-tone-3:' set="emojione" size={16} />*/}
                   </p>}
                   {isTyping&& (
                      <div className="message__typing">
                       <span />
                       <span />
                       <span />
                   </div>)}
                   {audio && (<MessageAudio audioSrc={audio}/>
                   )}
               </div>)}

                   {
                       attachments && (
                           <div className="message__attachments">
                               {attachments.map((item, index) => (
                           <div key={index} className="message__attachments-item">
                               <img src={item.url} alt={item.filename}/>
                           </div>
                          ))}
                           </div>
                       )}

               {date&& (
                   <span className="message__date">
                      <Time date={date}/>
                   </span>)
               }
           </div>
          </div>
       </div>
    )
};

Message.defaultProps = {
  user: {}
};

Message.prototype = {
    avatar: PropTypes.string,
    text: PropTypes.string,
    date: PropTypes.string,
    user: PropTypes.object,
    attachmets: PropTypes.array,
    isTyping: PropTypes.boolean,
    isMe: PropTypes.boolean,
    isReaded: PropTypes.boolean,
    audio: PropTypes.string
};

export default Message;