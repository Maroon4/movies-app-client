import React, {useState} from "react";
import PropTypes from "prop-types"
import {Button, Icon, Input} from "antd"
import { UploadField } from '@navjobs/upload'
import { Picker } from 'emoji-mart'

import "./ChatInput.scss"

const ChatInput = props => {

    const [value, setValue] = useState("");
    const [emojiPikerVisible, setShowEmojiPicker] = useState("");

    const toggleEmojiPiker = () => {
        setShowEmojiPicker(!emojiPikerVisible)
    };

    return (
        <div className="chat-input">
            <div className="chat-input__smile-btn">
                {
                    emojiPikerVisible && (
                        <div className="chat-input__emoji-piker">
                             <Picker set='emojione' />
                        </div>)
                }
                <Button
                    onClick={toggleEmojiPiker}
                    type="link"
                    shape="circle"
                    icon="smile"
                />
            </div>
            <Input
                onChange={e => setValue(e.target.value)}
                size="large"
                placeholder="Введіть текси"/>
            <div className="chat-input__actions">
                <UploadField
                    onFiles={files => console.log(files)}
                        containerProps={{
                        className: 'chat-input__actions-upload-btn'
                    }}
                        uploadProps={{
                        accept: '.jpg, .jpeg, .png, .gif',
                        multiple: "multiple"
                    }}
                 >
                    <Button type="link" shape="circle" icon="camera"/>
                </UploadField>
                {value ?
                    (<Button type="link" shape="circle" icon="check-circle"/>
                    ) : (<Button type="link" shape="circle" icon="audio"/> )}


            </div>
        </div>
    );

};

ChatInput.propTypes = {
    className: PropTypes.string
};

export default ChatInput;
