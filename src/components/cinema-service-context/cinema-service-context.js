import React from "react";

const {
    Provider: CinemastoreServiceProvider,
    Consumer: CinemastoreServiceConsumer
} = React.createContext();

export {
    CinemastoreServiceProvider,
    CinemastoreServiceConsumer
}