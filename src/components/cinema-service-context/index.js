import {
    CinemastoreServiceProvider,
    CinemastoreServiceConsumer
} from "./cinema-service-context";

export {
    CinemastoreServiceProvider,
    CinemastoreServiceConsumer
}